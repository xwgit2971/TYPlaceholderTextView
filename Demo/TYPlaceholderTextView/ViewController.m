//
//  ViewController.m
//  TYPlaceholderTextView
//
//  Created by 夏伟 on 2016/11/8.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "ViewController.h"
#import "TYPlaceholderTextView.h"

@interface ViewController () <UITextViewDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO; // Avoid the top UITextView space, iOS7 (~bug?)

    TYPlaceholderTextView *textView = [[TYPlaceholderTextView alloc] initWithFrame:CGRectMake(15, 100, CGRectGetWidth(self.view.frame)-30, 150)];
    textView.layer.masksToBounds = YES;
    textView.layer.cornerRadius = 5.0;
    textView.layer.borderWidth = 0.5;
    textView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    textView.backgroundColor = [UIColor whiteColor];
    textView.font = [UIFont systemFontOfSize:16];
    textView.placeholderColor = [UIColor redColor];
    textView.placeholder = @"请输入您宝贵的意见和建议，这对我们非常有帮助";
    textView.delegate = self;
    [self.view addSubview:textView];
}

@end
