//
//  TYPlaceholderTextView.h
//  TYPlaceholderTextView
//
//  Created by 夏伟 on 2016/10/20.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TYPlaceholderTextView : UITextView

@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) UIColor *placeholderColor;

@end
